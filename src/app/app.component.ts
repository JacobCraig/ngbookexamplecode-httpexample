import { Component } from '@angular/core';
import { SearchResult } from './models/searchresult.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public results: SearchResult[];

  public updateResults = (newResults: SearchResult[]) => {
    this.results = newResults;
  }
}
