import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { YoutubeItemComponent } from './youtube-item/youtube-item.component';
import { YoutubeSearchService } from './youtube-search.service';

@NgModule({
  declarations: [
    AppComponent,
    SearchBarComponent,
    YoutubeItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    YoutubeSearchService,
    { provide: 'YOUTUBE_API_KEY', useValue: 'AIzaSyDOfT_BO81aEZScosfTYMruJobmpjqNeEk' },
    { provide: 'YOUTUBE_API_URL', useValue: 'https://www.googleapis.com/youtube/v3/search' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
