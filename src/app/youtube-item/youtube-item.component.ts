import { Component, OnInit, Input } from '@angular/core';
import { SearchResult } from '../models/searchresult.model';

@Component({
  selector: 'youtube-item',
  templateUrl: './youtube-item.component.html',
  styleUrls: ['./youtube-item.component.scss']
})
export class YoutubeItemComponent {
  @Input() item: SearchResult;

  constructor() { }
}
