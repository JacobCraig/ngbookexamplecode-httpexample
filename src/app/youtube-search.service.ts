import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SearchResult } from './models/searchresult.model';

@Injectable({
  providedIn: 'root'
})
export class YoutubeSearchService {
  constructor(
    private http: HttpClient,
    @Inject('YOUTUBE_API_KEY') private apiKey: string,
    @Inject('YOUTUBE_API_URL') private apiUrl: string,
  ) {
  }

  public search = (query: string): Observable<SearchResult[]> => {
    var params: string = [
      `q=${query}`,
      `key=${this.apiKey}`,
      `part=snippet`,
      `type=video`,
      `maxResults=10`,
    ].join('&');

    return this.http.get(`${this.apiUrl}?${params}`).pipe(map(response => {
      return <any>response['items'].map(item => {
        return new SearchResult({
          id: item.id.videoId,
          title: item.snippet.title,
          description: item.snippet.description,
          thumbnailUrl: item.snippet.thumbnails.high.url,
        });
      });
    }));
  }
}
