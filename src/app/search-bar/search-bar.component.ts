import { Component, Output, EventEmitter, OnInit, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { YoutubeSearchService } from '../youtube-search.service';
import { SearchResult } from '../models/searchresult.model';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, tap, switchAll, switchMap } from 'rxjs/operators';

@Component({
  selector: 'search-bar',
  template: `
    <input class="form-control" type="text" autofocus />
  `,
})
export class SearchBarComponent implements OnInit {
  @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() results: EventEmitter<SearchResult[]> = new EventEmitter<SearchResult[]>();

  public searchForm: FormGroup;

  constructor(
    private YoutubeSearchService: YoutubeSearchService,
    private ElementRef: ElementRef,
  ) {
  }

  public search = (query: string) => {
    this.loading.emit(true);

    this.YoutubeSearchService.search(query).subscribe((results) => {
      this.results.emit(results);
      this.loading.emit(false);
    });
  }

  ngOnInit(): void {
    var asdf = fromEvent(this.ElementRef.nativeElement, 'keyup');
    asdf
      .pipe(
        map((e: any) => e.target.value),
        filter((text: string) => text.length > 1),
        debounceTime(250),
        tap(() => this.loading.emit(true)),
        switchMap((query: string) => this.YoutubeSearchService.search(query)),
      )
      .subscribe((results: any) => {
        this.results.emit(results);
        this.loading.emit(false);
      }, (err) => {
        console.log('error: ', err);
        this.loading.emit(false);
      }, () => {
        this.loading.emit(false);
      });
  }
}
